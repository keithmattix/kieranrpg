﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Xml.Serialization;
using System.Xml;
using Microsoft.Xna.Framework.Content;
using System.IO;
using KieranRPGLibrary;

namespace KieranRPG
{
    public class NPC : Character
    {
        private Command defaultCommand;
        private Queue<Command> commands = new Queue<Command>();
        private NPCSettings settings;
        private MapSprite sprite;

        public NPC(Game game, NPCSettings settings)
            : base(game)
        {
            this.settings = settings;
            this.Name = settings.Name;
            this.sprite = new MapSprite(this);
        }


        public MapSprite Sprite
        {
            get { return sprite; }
            set { sprite = value; }
        }


        /// <summary>
        /// Dequeues the first command in the queue then executes it
        /// The dictionary of arguments comes from
        /// </summary>
        public void execute()
        {
            if (commands.Count <= 0)
            {
                defaultCommand.Invoke(settings.Properties); 
            }
            commands.Dequeue().Invoke(settings.Properties);
        }


        public Queue<Command> Commands
        {
            get
            {
                return commands;
            }
            set
            {
                commands = value;
            }
        }

        public Command DefaultCommand
        {
            get
            {
                return defaultCommand;
            }
            set
            {
                defaultCommand = value;
            }
        }

        public override Camera Camera
        {
            get { return camera; }
            set { camera = value; }
        }

        public override void Update(GameTime gameTime)
        {
            
        }

        public override void Draw(GameTime gameTime)
        {
        }

        public override string Name
        {
            get { return settings.Name; }
            set { settings.Name = value; }
        }

        public override Rectangle Bounds
        {
            get { return settings.Bounds; }
            set { settings.Bounds = value; }
        }

        public override Vector2 Position
        {
            get { return settings.Position; }
            set { settings.Position = value; }
        }
    }

}

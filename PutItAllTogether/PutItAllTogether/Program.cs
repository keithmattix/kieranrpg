using System.Drawing;
using System.Xml;
using System.Xml.Serialization;
using FuncWorks.XNA.XTiled;
namespace KieranRPG
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (RPG game = new RPG())
            {

                game.Run();
            }
        }
    }
#endif
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using FuncWorks.XNA.XTiled;
using Microsoft.Xna.Framework.Graphics;
using KieranRPGLibrary;
using Microsoft.Xna.Framework.Content;

namespace KieranRPG.GameStates
{
    public class MapState : GameState
    {
        public static RPG gameReference;
        public static Player samplePlayer;
        private List<GameComponent> childComponents;

        public MapState(Game game, GameStateManager manager, Map stateMap) : base(game, manager)
        {
            this.map = stateMap;
            gameReference = (RPG)game;
            childComponents = new List<GameComponent>();
            foreach (ObjectLayer layer in map.ObjectLayers.AsEnumerable<ObjectLayer>())
            {
                MapObject[] objects = layer.MapObjects;
                foreach (MapObject mapObject in objects.AsEnumerable<MapObject>())
                {
                    if (mapObject.Type == "Player")
                    {
                        PlayerSettings settings = new PlayerSettings();
                        settings.Properties = mapObject.Properties;
                        //settings.Sprite.Animations.Add(MapSprite.State.Left
                        settings.Name = mapObject.Name;
                        settings.Position = new Vector2(mapObject.Bounds.X, mapObject.Bounds.Y);
                        samplePlayer = new Player(gameReference, settings);
                        samplePlayer.Sprite.SourceSpriteSheet.FrameSize = new Point(Map.TileWidth, Map.TileHeight);
                        samplePlayer.Sprite.SourceSpriteSheet.SourceTexture = gameReference.Content.Load<Texture2D>(@"Characters/" + mapObject.Properties["SourceSpriteSheet"].Value);
                        childComponents.Add(samplePlayer);
                    }
                    else if (mapObject.Type == "NPC")
                    {
                    }
                }
            }
        }

        Map map;

        public Map Map
        {
            get { return map; }
            set { map = value; }
        }

        protected override void LoadContent()
        {
            ContentManager Content = gameReference.Content;

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            foreach (GameComponent component in childComponents)
            {
                component.Update(gameTime);
            }
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            Camera camera = gameReference.Components.OfType<Camera>().First<Camera>();
            gameReference.spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, camera.Transform);
            map.Draw(gameReference.spriteBatch, new Rectangle(0, 0, gameReference.GraphicsDevice.Viewport.Width, gameReference.GraphicsDevice.Viewport.Height));
            DrawableGameComponent drawComponent;
            foreach (GameComponent component in childComponents)
            {
                if (component is DrawableGameComponent)
                {
                    drawComponent = component as DrawableGameComponent;
                    if (drawComponent.Visible)
                    {
                        drawComponent.Draw(gameTime);
                    }
                }
            }
            gameReference.spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuncWorks.XNA.XTiled;

namespace KieranRPGLibrary
{
    public abstract class Character : NeoMapObject
    {
        public Camera camera;
        public Game gameReference;

        public abstract Camera Camera
        {
            get;
            set;
        }

        public Character(Game game) : base(game)
        {

        }

        public abstract string Name
        {
            get;
            set;
        }
    }
}

﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KieranRPGLibrary
{
    public abstract class Sprite
    {
        public abstract Dictionary<MapSprite.State, Animation> Animations
        {
            get;
            set;
        }


        public abstract SpriteSheet SourceSpriteSheet
        {
            get;
            set;
        }
    }
}

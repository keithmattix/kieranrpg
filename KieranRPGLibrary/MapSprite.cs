﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;

namespace KieranRPGLibrary
{
    public delegate void AnimationChangedEventHandler(Animation newAnimation);
    public class MapSprite : Sprite
    {
        private SpriteSheet sourceSpriteSheet;
        private Dictionary<State, Animation> animations;
        private Animation currentAnimation;
        public event AnimationChangedEventHandler AnimationChanged;
        private NeoMapObject mapObject;


        public MapSprite(NeoMapObject mapObject)
        {
            animations = new Dictionary<State, Animation>();
            sourceSpriteSheet = new SpriteSheet();
            this.mapObject = mapObject;
        }

        public enum State
        {
            Idle_Up = 0,
            Idle_Down,
            Idle_Left,
            Idle_Right,
            Up,
            Down,
            Left,
            Right
        }


        public override Dictionary<State, Animation> Animations
        {
            get { return animations; }
            set { animations = value; }
        }

        public override SpriteSheet SourceSpriteSheet
        {
            get { return sourceSpriteSheet; }
            set { sourceSpriteSheet = value; }
        }


        public Animation CurrentAnimation
        {
            get { return currentAnimation; }
            set { ChangeAnimations(value); }
        }

        public void ChangeAnimations(Animation newAnimation)
        {
            currentAnimation = newAnimation;
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            currentAnimation.Draw(gameTime, spriteBatch, mapObject.Position);
        }

        public void Update(GameTime gameTime)
        {
            currentAnimation.Update(gameTime);
        }
    }
}

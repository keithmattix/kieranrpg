﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using KieranRPGLibrary;
using FuncWorks.XNA.XTiled;

namespace KieranRPGLibrary
{
    public class PlayerSettings
    {
        private Vector2 position;
        private string name;
        private Dictionary<String, Property> properties;
        private Rectangle bounds;

        public PlayerSettings()
        {
            name = "";
            properties = new Dictionary<string, Property>();
        }
        
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public Dictionary<string, Property> Properties
        {
            get { return properties; }
            set { properties = value; }
        }

        public Rectangle Bounds
        {
            get { return bounds; }
            set { bounds = value; }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using FuncWorks.XNA.XTiled;

namespace KieranRPGLibrary
{
    public class Camera : GameComponent
    {
        protected Rectangle viewport;
        private Game gameReference;
        private Player focus;
        private Map map;

        public Camera(Game game, Player focus)
            : base(game)
        {
            gameReference = game;
            viewport = new Rectangle(0, 0, Game.GraphicsDevice.Viewport.Width, Game.GraphicsDevice.Viewport.Height);
            this.focus = focus;
        }

        public Rectangle Viewport
        {
            get { return viewport; }
        }

   
        public float MoveSpeed { get; set; }
        public float Rotation { get; set; }
        Vector2 Origin { get; set; }
        public float Scale { get; set; }
        public Vector2 ScreenCenter { get; protected set; }
        public Matrix Transform { get; set; }
        public Player Focus
        {
            get { return focus; }
            set { focus = value; }
        }

        public override void Initialize()
        {
            ScreenCenter = new Vector2(viewport.Width / 2, viewport.Height / 2);
            Scale = 1;
            MoveSpeed = 1.25f;
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            Transform = Matrix.Identity *
                        Matrix.CreateTranslation(focus.Position.X, focus.Position.Y, 0) *
                        Matrix.CreateRotationZ(Rotation) *
                        Matrix.CreateScale(Scale);
            

            var delta = (float)gameTime.ElapsedGameTime.Milliseconds;
            

            if (InputHandler.KeyDown(Keys.S))
            {
                focus.Y -= (MoveSpeed / 16f) * delta;
                if (focus.Sprite.CurrentAnimation == focus.Sprite.Animations[MapSprite.State.Idle_Down])
                {
                    MapSprite sprite = focus.Sprite;
                    sprite.CurrentAnimation = focus.Sprite.Animations[MapSprite.State.Down];
                    
                }
                else
                {
                    focus.Sprite.CurrentAnimation = focus.Sprite.Animations[MapSprite.State.Idle_Down];
                }

            }
            if (InputHandler.KeyDown(Keys.W))
            {
                focus.Y += (MoveSpeed / 16f) * delta;
                if (focus.Sprite.CurrentAnimation == focus.Sprite.Animations[MapSprite.State.Idle_Up])
                {
                    MapSprite sprite = focus.Sprite;
                    sprite.CurrentAnimation = focus.Sprite.Animations[MapSprite.State.Up];
                }
                else
                {
                    focus.Sprite.CurrentAnimation = focus.Sprite.Animations[MapSprite.State.Idle_Up];
                }
            }
            if (InputHandler.KeyDown(Keys.D))
            {
                focus.X += (MoveSpeed / 16f) * delta;
                if (focus.Sprite.CurrentAnimation == focus.Sprite.Animations[MapSprite.State.Idle_Right])
                {
                    MapSprite sprite = focus.Sprite;
                    sprite.CurrentAnimation = focus.Sprite.Animations[MapSprite.State.Right];
                }
                else
                {
                    focus.Sprite.CurrentAnimation = focus.Sprite.Animations[MapSprite.State.Idle_Right];
                }
            }
            if (InputHandler.KeyDown(Keys.A))
            {
                focus.X = (MoveSpeed / 16f) * delta;
                if (focus.Sprite.CurrentAnimation == focus.Sprite.Animations[MapSprite.State.Idle_Left])
                {
                    MapSprite sprite = focus.Sprite;
                    sprite.CurrentAnimation = focus.Sprite.Animations[MapSprite.State.Left];
                }
                else
                {
                    focus.Sprite.CurrentAnimation = focus.Sprite.Animations[MapSprite.State.Idle_Left];
                }
            }
            

        }

        public bool IsInView(Vector2 position, Texture2D texture)
        {
            if ((position.X + texture.Width) < (focus.X - Origin.X) || (position.X) > (focus.X + Origin.X))
            {
                return false;
            }

            if ((position.Y + texture.Height) < (focus.Y - Origin.Y) || (position.Y) > (focus.Y + Origin.Y))
            {
                return false;
            }
            return true;
        }
    }
}

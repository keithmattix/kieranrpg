﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KieranRPGLibrary
{
    interface IExecutable
    {
        void execute();

        Queue<Command> Commands
        {
            get;
        }

        Command DefaultCommand
        {
            get;
            set;
        }
    }
}

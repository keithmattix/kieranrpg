﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KieranRPGLibrary
{
    public class Frame
    {
        private Rectangle sourceRectangle;
        private SpriteSheet sourceSpriteSheet;
        private int duration;

        public Frame()
        {
            duration = 3000;
        }
        public int Duration
        {
            get { return duration; }
        }

        [Microsoft.Xna.Framework.Content.ContentSerializerIgnore]
        public SpriteSheet SourceSpriteSheet
        {
            get { return sourceSpriteSheet; }
            set { sourceSpriteSheet = value; }
        }

        public Rectangle SourceRectangle
        {
            get { return sourceRectangle; }
            set { sourceRectangle = value; }
        }

        public Frame(SpriteSheet sourceSpriteSheet, Rectangle sourceRectangle, int duration)
        {
            this.sourceSpriteSheet = sourceSpriteSheet;
            this.sourceRectangle = sourceRectangle;
            this.duration = duration;
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, Rectangle position)
        {
            spriteBatch.Draw(SourceSpriteSheet.SourceTexture, position, sourceRectangle, Color.White);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, Vector2 position)
        {
            spriteBatch.Draw(SourceSpriteSheet.SourceTexture, position, sourceRectangle, Color.White);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace KieranRPGLibrary
{
    public class Player : Character
    {
        private PlayerSettings settings;
        private MapSprite sprite;
        private Vector2 position;

        public Player(Game game, PlayerSettings settings) : base(game)
        {
            this.gameReference = game;
            this.settings = settings;
            this.sprite = new MapSprite(this);
            Frame frame = new Frame(sprite.SourceSpriteSheet, new Rectangle(0, 0, 64, 96), 120);
            List<Frame> singleFrameList = new List<Frame>() { frame };
            Animation walkDown = game.Content.Load<Animation>(@"WalkDown");
            walkDown.SourceSpriteSheet = sprite.SourceSpriteSheet;
            walkDown.DelayInit();
            foreach(Frame f in walkDown.Frames){
                f.SourceSpriteSheet = sprite.SourceSpriteSheet;
            }
            Animation idleDown = new Animation(singleFrameList, sprite.SourceSpriteSheet);
            sprite.Animations.Add(MapSprite.State.Down, walkDown);
            sprite.Animations.Add(MapSprite.State.Idle_Down, idleDown);
            sprite.CurrentAnimation = sprite.Animations[MapSprite.State.Idle_Down];
        }


        public MapSprite Sprite
        {
            get { return sprite; }
            set { sprite = value; }
        }

        public override Camera Camera
        {
            get { return camera; }
            set { camera = value; }
        }

        public override void Update(GameTime gameTime)
        {
            Console.Write("(" + position.X + "," + position.Y + ")");
            sprite.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = new SpriteBatch(gameReference.GraphicsDevice);
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, camera.Transform);
            sprite.Draw(gameTime, spriteBatch);
            spriteBatch.End();
        }

        public override Rectangle Bounds
        {
            get { return settings.Bounds; }
            set { settings.Bounds = value; }
        }

        public override string Name
        {
            get { return settings.Name; }
            set { settings.Name = value; }
        }

        public override Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        public float X
        {
            get { return position.X; }
            set { position.X = value; }
        }

        public float Y
        {
            get { return position.Y; }
            set { position.Y = value; }
        }
        
    }
}

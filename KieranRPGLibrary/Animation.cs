﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KieranRPGLibrary
{
    public class Animation
    {
        private List<Frame> frames;
        private bool loop;
        private SpriteSheet sourceSpriteSheet;
        private int currentFrameIndex = 0;
        private int delay;

        public List<Frame> Frames
        {
            get { return frames; }
            set { frames = value; }
        }

        public bool Loop
        {
            get { return loop; }
            set { loop = value; }
        }

        [Microsoft.Xna.Framework.Content.ContentSerializerIgnore]
        public SpriteSheet SourceSpriteSheet
        {
            get { return sourceSpriteSheet; }
            set { sourceSpriteSheet = value; }
        }

        public void DelayInit()
        {
            delay = frames[currentFrameIndex].Duration;
        }

        public Animation(List<Frame> frames, SpriteSheet sourceSpriteSheet) : this(frames, sourceSpriteSheet, false) { }

        public Animation(List<Frame> frames, SpriteSheet sourceSpriteSheet, bool loop)
        {
            this.frames = frames;
            this.sourceSpriteSheet = sourceSpriteSheet;
            this.loop = loop;
            currentFrameIndex = 0;
            delay = frames[currentFrameIndex].Duration;
        }

        public Animation()
        {
        }

        public void Update(GameTime gameTime)
        {
            delay -= gameTime.ElapsedGameTime.Milliseconds;
            if(currentFrameIndex < frames.Count - 1)
            {
              currentFrameIndex++;
            } 
            else if(loop)
            {
             currentFrameIndex = 0;
            }
            else
            {
             currentFrameIndex = frames.Count - 1;
            }
            delay = frames[currentFrameIndex].Duration;
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, Rectangle position)
        {
            frames[currentFrameIndex].Draw(gameTime, spriteBatch, position);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, Vector2 position)
        {
            frames[currentFrameIndex].Draw(gameTime, spriteBatch, position);
        }
    }
}
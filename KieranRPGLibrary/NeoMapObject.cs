﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KieranRPGLibrary
{
    public abstract class NeoMapObject : DrawableGameComponent
    {
        public NeoMapObject(Game game)
            : base(game)
        {

        }

        public abstract Rectangle Bounds
        {
            get;
            set;
        }


        public abstract Vector2 Position
        {
            get;
            set;
        }


        protected override void LoadContent()
        {
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
    }
}

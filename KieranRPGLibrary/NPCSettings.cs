﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using FuncWorks.XNA.XTiled;

namespace KieranRPGLibrary
{
    public class NPCSettings
    {
        private Vector2 position;
        private MapSprite sprite;
        private string name;
        private Dictionary<String, Property> properties;

        public NPCSettings()
        {
            sprite = new MapSprite();
            name = "";
            properties = new Dictionary<string, Property>();
        }
        
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        public MapSprite Sprite
        {
            get { return sprite; }
            set { sprite = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [ContentSerializerIgnore]
        public Dictionary<string, Property> Properties
        {
            get { return properties; }
            set { properties = value; }
        }

    }
}

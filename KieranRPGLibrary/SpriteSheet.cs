﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KieranRPGLibrary
{
    public class SpriteSheet
    {
        private Texture2D sourceTexture;
        private string sourceFileName;
        private Point frameSize;
        private Point sheetSize;

        public SpriteSheet(Texture2D sourceTexture, Point size)
        {
            this.sourceTexture = sourceTexture;
            this.sheetSize = size;
            this.frameSize = new Point(sourceTexture.Width / size.X, sourceTexture.Height / size.Y);
        }

        public SpriteSheet()
        {
        }

        public string SourceFileName
        {
            get { return sourceFileName; }
            set { sourceFileName = value; }
        }

        public Point FrameSize
        {
            get { return frameSize; }
            set { frameSize = value; }
        }

        public Point SheetSize
        {
            get { return sheetSize; }
        }

        public Texture2D SourceTexture
        {
            get { return sourceTexture; }
            set { sourceTexture = value; assignValues(); }
        }

        private void assignValues()
        {
            if (sourceTexture != null)
            {
                if (frameSize != null)
                {
                    sheetSize = new Point(sourceTexture.Width / frameSize.X, sourceTexture.Height / frameSize.Y);
                }
                sourceFileName = sourceTexture.Name;
                
            }
        }
    }
}
